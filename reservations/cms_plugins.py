#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals


from django import forms
from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from reservations import models as m
from reservations import forms as f
from django.utils import timezone

class CMSReservationPlugin(CMSPluginBase):
    model = m.ReservationPlugin
    name = "Бронирование"
    render_template = "reservations/cms_templates/reservation_plugin.html"

    def render(self, context, instance, placeholder):
        form = f.PreOrderForm()
        if instance.performance:
            form.set_performance(instance.performance.pk)
        if instance.place:
            form.set_place(instance.place.pk)

        context.update({
            'instance': instance,
            'form': form,
            })
        return context

class CMSSchedulePlugin(CMSPluginBase):
    model = m.SchedulePlugin
    name = "Превью афиши"
    render_template = "reservations/cms_templates/schedule_plugin.html"

    def render(self, context, instance, placeholder):
        context['object_list'] = m.Schedule.playbill_set.all()[:instance.number]
        return context

plugin_pool.register_plugin(CMSReservationPlugin)
plugin_pool.register_plugin(CMSSchedulePlugin)
