#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals


from logging import getLogger
log = getLogger('reservations.views')

from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView
from django.views.generic.list import BaseListView
from django.views.generic.edit import UpdateView, FormView, DeleteView
from django.http import Http404, HttpResponse
from django.core.exceptions import PermissionDenied, FieldError
from django.core.urlresolvers import reverse, reverse_lazy
from django.core import serializers
from django.contrib.auth import logout
from django.shortcuts import redirect

from reservations import models as m
from reservations import forms as f
from reservations.sessions import SpectatorSession

from user_profile.views import build_register_link


# ======================================
# Reservation signal
# ======================================

from django.dispatch import Signal, receiver

from reservations.comm import ConfirmationMessage, NotificationMessage
reservation_made = Signal(providing_args=['reservation'])

@receiver(reservation_made)
def send_confirmation(sender, **kwargs):
    """
    Send confirmation email to spectator
    """
    log.debug("Sending confirmation to spectator")
    reservation = kwargs['reservation']
    to = [reservation.spectator.email]
    tmpl = reservation.show.place.confirmation_body
    confirmation = ConfirmationMessage(
            body_template = tmpl,
            to=to,
            reference_number = reservation.reference_number,
            spectator=reservation.spectator,
            show=reservation.show,
            invoice=dict(
                    total=reservation.total_cost
                    ),
            manager = reservation.show.place.manager,
            )
    confirmation.send()

@receiver(reservation_made)
def notify_manager(sender, **kwargs):
    """
    Send notification to manager
    """
    log.debug("Sending notification to manager 1")
    reservation = kwargs['reservation']
    log.debug("reservation is %s, place is %s, manager is %s",
            reservation, reservation.show.place, reservation.show.place.manager)
    manager = reservation.show.place.manager
    if manager is not None:
        log.debug("Sending notification to manager 2")
        notification = NotificationMessage(
                to=[manager.email],
                reference_number = reservation.reference_number,
                comments = reservation.additional_info,
                spectator=reservation.spectator,
                show=reservation.show,
                reservation=reservation,
                seating_child=reservation.seating_child + 1,
                seating_adult=reservation.seating_adult + 1,
                invoice=dict(
                    total=reservation.total_cost
                    )
                )
        notification.send()

# Create your views here.


class ScheduleView(ListView):
    model = m.Schedule
    template_name = "reservations/index.html"
    queryset = m.Schedule.playbill_set.extra(
        select={'showdate': 'DATE(showtime)'}).extra(
            order_by=('showdate', 'place', 'showtime'))


class PlaceView(DetailView):
    model = m.Place
    template_name = "reservations/place.html"
    def get_context_data(self, **kwargs):
        spect_session = SpectatorSession(self.request)
        context = super(PlaceView, self).get_context_data(**kwargs)
        form = f.PreOrderForm().set_spectator(spect_session.spectator)
        if form.set_place(context['object'].pk) is not None:
            context['form'] = form
        return context

class PlaceListView(ListView):
    model = m.Place
    tamplate_name = "reservations/place_list.html"

class PerformanceView(DetailView):
    model = m.Performance
    template_name = "reservations/performance.html"

    def get_context_data(self, **kwargs):
        spect_session = SpectatorSession(self.request)
        context = super(PerformanceView, self).get_context_data(**kwargs)
        form = f.PreOrderForm().set_spectator(spect_session.spectator)
        if form.set_performance(context['object'].pk) is not None:
            context['form'] = form
        return context


class BookingMixin(object):
    """
    Holds utility funcs for BookingView
    """
    _show_qs = None

    def update_instance(self, data, obj):
        update_dict = {k:data[k] for k in data if data[k] and
                k in [f.name for f in obj._meta.fields]}
        model = obj._meta.model
        if update_dict:
            model.objects.filter(id=obj.id).update(**update_dict)
        return model.objects.get(id=obj.id)

    def get_show_qs(self, **kwargs):
        show_qs = m.Schedule.available.all()
        if 'performance_pk' in kwargs:
            show_qs = show_qs.filter(
                performance__pk=kwargs.get('performance_pk'),
                )
        if 'place_pk' in kwargs:
            show_qs = show_qs.filter(
                    place__pk=kwargs['place_pk'],
                    )

        return show_qs

    def create_reservation(self, spect_session):
        if not spect_session.spectator:
            spectator = m.Spectator(**spect_session.spectator_data)
            spectator.save()
            spect_session.set_spectator(spectator)
        else:
            log.debug("Spectator is %s",
                    spect_session.spectator)
            log.debug("spectator_data is %s",
                    spect_session.spectator_data)
            spectator = self.update_instance(
                    spect_session.spectator_data,
                    spect_session.spectator
                    )

        return m.Reservation(
                    spectator=spectator,
                    )

    def send_signal(self, reservation):
        reservation_made.send(self, reservation=reservation)



class BookingView(FormView, BookingMixin):
    """
    Process booking data and redirect to ThankYou
    Create Spectator if necessary
    """

    template_name = "reservations/booking.html"
    form_class = f.BookingForm
    additional_context = {}
    success_url = reverse_lazy("thank_you")

    def get_form(self, form_class):
        form = super(BookingView, self).get_form(form_class)
        spect_session = SpectatorSession(self.request)
        if spect_session.spectator:
            # spectator was identified: email should be filled initially
            form.fields['email'].initial = spect_session.spectator.email
            log.debug("Setting email initial to %s",
                    spect_session.spectator.email)
        elif "email" in spect_session.spectator_data:
            # email was supplied before, should be filled initially
            form.fields['email'].initial =\
                    spect_session.spectator_data['email']
            log.debug("Setting email initial to %s",
                    spect_session.spectator_data['email'])
        if spect_session.user:
            # spectator is registered (and logged in perhaps)
            form.remove_credentials()
        show_qs = self.additional_context["show_qs"]
        if show_qs.exists():
            form.set_show_qs(show_qs).set_show_initial(
                        self.additional_context.pop("show_pk"))
            # - if there are no available shows for the choosen combination,
            # than "booking not available" message should appear
            return form

    def dispatch(self, request, *args, **kwargs):
        if 'show_pk' in kwargs:
            # show was choosed before
            show = get_object_or_404(m.Schedule, pk=kwargs['show_pk'])
            #  ---------- [Check for possible redirection here] ----------
            if show.has_external_booking:
                return redirect(show.external_booking_url)


            # ------------------------------------------------------------
            kwargs['performance_pk'] = show.performance.pk
            kwargs['place_pk'] = show.place.pk

        self.additional_context['show_qs'] = self.get_show_qs(**kwargs)
        self.additional_context['show_pk'] = self.request.POST.get(
                'show', None) or kwargs.get('show_pk', None)
        self.additional_context['performance'] = get_object_or_404(
                m.Performance, pk=kwargs['performance_pk'])
        self.additional_context['place'] = get_object_or_404(
                m.Place, pk=kwargs['place_pk'])

        return super(BookingView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        kwargs.update(self.additional_context)
        return super(BookingView, self).get_context_data(**kwargs)

    def form_invalid(self, form):
        show = form.cleaned_data.pop('show', None)
        #  ---------- [Check for possible redirection here] ----------
        if show is not None and show.has_external_booking:
            return redirect(show.external_booking_url)

        # ------------------------------------------------------------
        return super(BookingView, self).form_invalid(form)

    def form_valid(self, form):
        show = form.cleaned_data.pop('show')
        #  ---------- [Check for possible redirection here] ----------
        if show.has_external_booking:
            return redirect(show.external_booking_url)

        # ------------------------------------------------------------
        spect_session = SpectatorSession(self.request, form.cleaned_data)
        reservation = self.create_reservation(
                spect_session
                )
        reservation.show = show
        reservation.seating_adult = form.cleaned_data.pop('seating_adult') - 1
        reservation.seating_child = form.cleaned_data.pop('seating_child') - 1
        reservation.save()
        reservation = self.update_instance(
                form.cleaned_data,
                reservation
                )
        spect_session.set_reservation(reservation)
        self.send_signal(reservation)


        return super(BookingView, self).form_valid(form)

class BookingAPIView(BaseListView):
    """
    Gets places queryset, and sends it as json
    """

    def json_response(self, json_data, **response_kwargs):
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(json_data, **response_kwargs)

    def get_queryset(self):
        try:
            return m.Schedule.available.filter(**self.request.GET.dict())
        except FieldError:
            raise Http404("Bad request")

    def get(self, request, *args, **kwargs):
        log.debug("in get")
        dataset = kwargs['dataset']
        json_data = {}
        schedules = self.object_list = self.get_queryset()
        if dataset == 'performances':
            performances = m.Performance.objects.filter(
                pk__in=schedules.values_list('performance', flat=True)
            )
            json_data['performances'] = serializers.serialize(
                "json",
                performances,
                fields=('title', 'status', 'hide')
            )
        elif dataset == 'places':
            places = m.Place.objects.filter(
                pk__in=schedules.values_list('place', flat=True)
            )
            json_data['places'] = serializers.serialize(
                "json",
                places,
                fields=('title', 'status', 'hide')
            )
        elif dataset == 'schedules':
            json_data['schedules'] = serializers.serialize(
                "json",
                schedules
            )
        else:
            raise Http404("Bad dataset")
        return self.json_response(json_data[dataset])



class BookingPreorderView(FormView):
    """
    This should receive post from PreOrder form
    check preprocess data and redirect to BookingView
    """
    template_name = "reservations/preorder.html"
    form_class = f.PreOrderForm

    def form_valid(self, form):
        if form.cleaned_data.get("email", None):
            log.debug("BookingPreorder: injecting data to spectator session")
            SpectatorSession(self.request, form.cleaned_data)
        self.success_url = reverse('booking',
                kwargs = {
                    'performance_pk': form.cleaned_data['performance'].pk,
                    'place_pk': form.cleaned_data['place'].pk,
                 })
        return super(BookingPreorderView, self).form_valid(form)


class ThankYouMixin(object):
    def get_user_credentials(self, session):
        return session.spectator

    def get_email(self, session):
        return dict(
                email=session.spectator.email
                )

    def get_session_credentials(self, session):
        return session.spectator_data

    def get_user_actions(self, spectator_status, session=None):
        a = {'href': '', 'purpose': ''}
        kwargs = {}
        if spectator_status == "unregistered":
            # provide link to registration view
            # supply get with next=update_profile
            if session is not None:
                kwargs['email'] = session.spectator.email
            a['purpose'] = "register"
            a['href'] = build_register_link(
                    reverse("update_profile"),
                    **kwargs)
        elif spectator_status == "authenticated":
            a['purpose'] = "profile"
            a['href'] = reverse("profile")
        elif spectator_status == "registered":
            a['purpose'] = "login"
            a['href'] = reverse("login")
        return a


class ThankYouView(DetailView, ThankYouMixin):
    model = m.Reservation
    template_name = "reservations/thank-you.html"
    context_object_name = "reservation"
    spect_session = None

    def get_object(self):
        self.spect_session = SpectatorSession(self.request)
        log.debug("Spectator session in thank you: %s",
                self.spect_session)
        return self.spect_session.reservation

    def get_context_data(self, **kwargs):
        context = super(ThankYouView, self).get_context_data(**kwargs)
        session = self.spect_session
        if self.request.user.is_authenticated():
            # authenticated
            log.debug("\n%suser is authenticated", "-"*40)
            context['credentials'] = self.get_user_credentials(session)
            context['user_actions'] = self.get_user_actions(
                        "authenticated")
        else:
            if self.spect_session.user:
                # registered
                log.debug("\n%suser is registered", "-"*40)
                context['credentials'] = self.get_email(session)
                context['user_actions'] = self.get_user_actions(
                        "registered")
            elif self.spect_session.spectator:
                # unregistered
                log.debug("user is unregistered")
                context['credentials'] = self.get_session_credentials(session)
                context['user_actions'] = self.get_user_actions(
                        "unregistered", session)
            else:
                # unknown
                log.debug("\n%suser is unknown", "-"*40)
                raise Http404("Spectator session is empty")
        log.debug("ThankYou context: %s",  str(context).decode('utf-8'))
        return context


class SpectatorUpdateView(UpdateView):
    model = m.Spectator
    template_name = 'reservations/spectator_update.html'
    success_url = reverse_lazy("profile")

    fields = ("first_name", "last_name", "email", "tel", "children")

    def get_object(self, queryset=None):
        session = self.spect_session = SpectatorSession(self.request)
        log.debug("session spectator is %s", session.spectator)
        spectator = session.spectator
        user = self.request.user
        if spectator is None:
            if hasattr(self.request.user, "spectator"):
                return user.spectator
            else:
                raise PermissionDenied(
                        "Регистрация не удалась, попробуйте еще раз")
        if spectator.user is None:
            # user is authenticated, but not linked to profile
            # user was just registered
            log.debug("user email %s", user.email)
            # update spectator from session to avoid data leaks on register
            if user.email != spectator.email:
                # something is wrong
                logout(self.request)
                user.delete()
                raise PermissionDenied(
                        "Регистрация не удалась, попробуйте еще раз")
            self.model.objects.filter(pk=spectator.pk).update(
                    **session.spectator_data)

            # link to user
            self.model.objects.filter(pk=spectator.pk).update(
                    user=user,
                    )
        return self.model.objects.get(pk=spectator.pk)


class SpectatorProfileView(ListView):
    """
    Show spectator data, including his reservations
    """
    model = m.Reservation
    template_name = 'reservations/spectator_profile.html'

    def get_queryset(self):
        if hasattr(self.request.user, "spectator"):
            spectator = self.request.user.spectator
        else:
            return None
        return self.model.objects.filter(spectator=spectator)

    def get_context_data(self, **kwargs):
        context = super(SpectatorProfileView, self).get_context_data(**kwargs)
        if hasattr(self.request.user, "spectator"):
            context['spectator'] = self.request.user.spectator
        return context

class ReservationCancelView(DeleteView):
    model = m.Reservation
    success_url = reverse_lazy("profile")
    template_name = "reservations/reservation_delete.html"

    def get_object(self, queryset=None):
        reservation = super(ReservationCancelView, self).get_object(queryset)
        if hasattr(self.request.user, "spectator"):
            spectator = self.request.user.spectator
            if reservation.spectator == spectator:
                return reservation
        raise PermissionDenied("Вы не можете удалить чужую бронь")
