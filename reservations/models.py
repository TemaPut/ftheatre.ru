#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from logging import getLogger
log = getLogger("reservations.models")


from datetime import timedelta

from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.conf import settings

from cms.models.fields import PlaceholderField
from djangocms_text_ckeditor.fields import HTMLField

from .utils import print_local_datetime

# Create your models here.

@python_2_unicode_compatible
class PlaceManager(models.Model):
    """
    Менеджер площадки
    - привязан к пользователю
    - телефон
    """
    user = models.OneToOneField(
            User,
            verbose_name="Зарегистрирован в системе как",
            help_text="Менеджер должен быть зарегистирован как персонал сайта"
            )
    tel = models.CharField("Мобильный телефон", max_length=50,
            blank=True)

    def first_name(self):
        return self.user.first_name
    first_name.short_description = "Имя"
    def last_name(self):
        return self.user.last_name
    last_name.short_description = "Фамилия"
    def email(self):
        return self.user.email
    first_name = property(first_name)
    last_name = property(last_name)
    email = property(email)

    def __str__(self):
        return "%s %s" % (self.user.first_name,
                self.user.last_name)
    class Meta:
        verbose_name = "Менеджер"
        verbose_name_plural = "Менеджеры площадок"

@python_2_unicode_compatible
class AbstractPerformance(models.Model):
    """
    Спектакль
    - Название
    - Описание
    - Цена по умолчанию
    - Кол-во мест по умолчанию
    - Иконка
    - Статус (напр. идет или архив...)
    - Инструкции для зрителей (напр. захватите полотенце и носочки)
    """

    title = models.CharField("Название", max_length=256)
    description = HTMLField("Описание", blank=True)
    thumb = models.ImageField(upload_to="images/thumbs/perfomances/%Y/%m/",
            verbose_name="Аватарка",
            help_text="Небольшое фото, которое будет использоваться на "
            "кнопках, ведущих на страничку спектакля", blank=True)
    status = models.CharField("Вид", max_length=50, blank=True,
                              help_text="Например: Треннинг, Мастер-класс "
                              "(для неспектаклей)")
    brief = HTMLField("Инструкции для зрителей",blank=True,
            help_text="Например: захватите полотенце, дети будут играть "
            "с водой"
            )
    hide = models.BooleanField("Скрыть", help_text="Не показывать спектакль",
                               default=False)


    def get_absolute_url(self):
        return reverse(
                "performances",
                kwargs={'pk': self.pk},
                )

    def __str__(self):

        return "%s %s" % (self.status or "Спектакль", self.title)

    class Meta:
        abstract = True
        ordering = ["title"]
        verbose_name = "Спектакль"
        verbose_name_plural = "Спектакли"



@python_2_unicode_compatible
class AbstractPlace(models.Model):
    """
    Площадка
    - адрес
    - как добраться
    - менеджер (кого уведомлять, кому разрешено закрывать показы и т.д.)
    """
    title = models.CharField("Название", max_length=50)
    description = HTMLField("Описание", blank=True,
            help_text="Общее описание площадки")
    thumb = models.ImageField(upload_to="images/thumbs/places/%Y/%m/",
            verbose_name="Аватарка",
            help_text="Небольшое фото, которое будет использоваться на "
            "кнопках, ведущих на страничку площадки", blank=True)
    manager = models.ForeignKey(PlaceManager, blank=True, null=True,
            on_delete=models.SET_NULL, related_name='+',
            verbose_name="Менеджер",
            help_text="Человек, отвечающий за бронирование билетов для"
            " данной площадки (должен быть зарегистрирован на сайте)"
            )
    address = models.CharField("Адрес", max_length=256, blank=True)
    tel = models.CharField("Телефон", max_length=15, blank=True)
    how_to_get = HTMLField("Инструкции по бронированию", blank=True,
            help_text="Например: Стоимость указана за одного...")
    cancelling_body = models.TextField(
            "Как добраться",
            blank=True,
            )
    default_price = models.DecimalField("Стоимость (по умолчанию)",
            max_digits=8, decimal_places=2, default=0,
            help_text="Цена, которая будет автоматически подставляться "
            "при составлении расписания на месяц (но каждый раз может быть "
            "изменена вручную")
    default_price_adult = models.DecimalField("Стоимость доп. (взр)",
            max_digits=8, decimal_places=2, default=0,
            help_text="Цена, которая будет автоматически подставляться "
            "при составлении расписания на месяц (но каждый раз может быть "
            "изменена вручную")
    default_price_child = models.DecimalField("Стоимость доп. (детск)",
            max_digits=8, decimal_places=2, default=0,
            help_text="Цена, которая будет автоматически подставляться "
            "при составлении расписания на месяц (но каждый раз может быть "
            "изменена вручную")
    confirmation_body =  models.TextField(
            "Текст подтверждения брони",
            blank=True,
            help_text = """
    На Ваше имя оформлена бронь на спектакль {{ show.performance.title }}.
    Время и место проведения: {{ show.showtime|date:"l, j E H:i" }} на площадке {{ show.place.title }}
    Номер брони: {{ reference_number }}.
        """
            )
    payment_received_body = models.TextField(
            "Текст письма о поступившей оплате",
            blank=True,
            )

    status = models.CharField("Статус", max_length=50, blank=True,
            help_text="Например: больше не используется..."
            )
    hide = models.BooleanField("Скрыть", help_text="Не показывать площадку",
                               default=False)

    def get_absolute_url(self):
        return reverse(
                "places",
                kwargs={'pk': self.pk},
                )

    def __str__(self):
        return "Площадка %s" % self.title

    class Meta:
        abstract=True
        ordering=["title"]
        verbose_name = "Площадка"
        verbose_name_plural = "Площадки"


class Performance(AbstractPerformance):
    """
    Performance with cms placeholder
    """

    placeholder_field2 = PlaceholderField(
            "header_image",
            related_name="permormance_headers",
            )

    placeholder_field = PlaceholderField(
            "additional_content",
            related_name="permormance_additionals",
            )


class PerformanceProxy(Performance):
    """
    Proxy model to allow different admin interface
    """
    class Meta:
        proxy = True
        verbose_name = "Расписание спектаклей"
        verbose_name_plural = "Показы (составление)"

class Place (AbstractPlace):
    """
    Place with cms placeholder
    """
    placeholder_field2 = PlaceholderField(
            "header_image",
            related_name="place_headers",
            )

    placeholder_field = PlaceholderField(
            "additional_content",
            related_name="place_additionals",
            )


@python_2_unicode_compatible
class Spectator(models.Model):
    """
    Зритель
    - ФИО
    - моб. тел.
    - мэйл*
    - дата регистрации
    - дети -> Children
    - как узнали -> Promo
    """
    first_name = models.CharField("Имя", max_length=50,
            blank=True)
    last_name = models.CharField("Фамилия", max_length=50,
            blank=True)
    email = models.EmailField("E-mail", max_length=256,
            unique=True)
    tel = models.CharField("Мобильный телефон", max_length=50,
            blank=True)
    registered = models.DateField("Дата регистрации", auto_now_add=True)

    children = models.TextField("Дополнительные сведения",
            blank=True,
            help_text="Укажите возраст и имена детей")

    user = models.OneToOneField(
            settings.AUTH_USER_MODEL,
            verbose_name="Зритель зарегистрирован как",
            blank=True,
            null=True,
            )


    def __str__(self):
        return "Зритель %s %s" % (
                self.first_name,
                self.last_name or "-",
                )

    class Meta:
        ordering=["-registered"]
        get_latest_by = "registered"
        verbose_name = "Зритель"
        verbose_name_plural = "Зрители"


class Children(models.Model):
    """
    Дети (не используется)
    - Имя
    - Возраст
    - Родитель -> Зритель
    name = models.CharField("Имя", max_length=100)
    age = models.DecimalField("Возраст", max_digits=3, decimal_places=1)
    parent = models.ForeignKey(Spectator, verbose_name="Родитель",
            related_name="children")

    def __str__(self):
        return "Ребенок %s" % self.title

    class Meta:
        verbose_name = "Ребенок"
        verbose_name_plural = "Дети"
    """
    pass

class InPlaybillScheduleManager(models.Manager):
    """
    Manager that returns only rows that are not outdated
    """

    def get_query_set(self):
        return super(InPlaybillScheduleManager, self).get_query_set().filter(
                showtime__gte=timezone.now(),
                )

class AvailableScheduleManager(models.Manager):
    """
    Manager that returns only rows that are not outdated and not sold
    """

    def get_query_set(self):
        return super(AvailableScheduleManager, self).get_query_set().filter(
                showtime__gte=timezone.now(),
                sold_out=False,
                )

class ScheduleAdminManager(models.Manager):
    """
    Manager that returns only rows that are not outdated
    """

    def get_query_set(self):
        return super(ScheduleAdminManager, self).get_query_set().filter(
                showtime__gte=timezone.now() - timedelta(hours=12))


@python_2_unicode_compatible
class Schedule(models.Model):
    """
    Расписание показов
    - Спектакль
    - Площадка
    - Дата и время показа
    - Стоимость билета
    - Мест нет
    """
    performance = models.ForeignKey(Performance,
            verbose_name="Спектакль",
            related_name="shows_scheduled")
    place = models.ForeignKey(Place,
            verbose_name="Площадка",
            related_name="shows_scheduled",
            limit_choices_to={'hide': False})
    showtime = models.DateTimeField("Время представления",
            help_text="Когда и во сколько состоится данный показ")
    price = models.DecimalField("Стоимость основного билета",
            max_digits=8, decimal_places=2, blank=True, null=True,
            help_text="если не заполнить, будет использована стоимость по-умолчанию"
            )
    price_adult = models.DecimalField("Стоимость доп. билета (взр)",
            max_digits=8, decimal_places=2, blank=True, null=True,
            help_text="если не заполнить, будет использована стоимость по-умолчанию"
            )
    price_child = models.DecimalField("Стоимость доп. билета (детск)",
            max_digits=8, decimal_places=2, blank=True, null=True,
            help_text="если не заполнить, будет использована стоимость по-умолчанию"
            )
    sold_out = models.BooleanField("Мест нет", default=False,
            help_text="Поставить галочку, когда все билеты будут"
            " забронированы")
    external_booking_url = models.URLField(
        "Ссылка внешнего бронирования",
        blank=True,
        help_text="Заполнять только если бронирование производитеся на другом"
        " сайте (например, ЦИМ)"
    )

    @property
    def has_external_booking(self):
        return len(self.external_booking_url) > 0



    def is_available(self):
        return self in self.__class__.available.all()
    is_available.short_description="Доступен для заказа"
    is_available.boolean=True
    #is_available = property(is_available)

    # Model managers
    objects = ScheduleAdminManager()
    available = AvailableScheduleManager()
    playbill_set = InPlaybillScheduleManager()
    complete_object_list = models.Manager()



    def reservations_made(self):
        """
        first tickets = 1 adult + 1 child
        + additional tickets
        """
        first_tickets = self.reservations.filter(cancelled=False).count() * 2
        additional_tickets = \
            self.reservations.filter(cancelled=False).aggregate(
                models.Sum('seating_adult'), models.Sum('seating_child'))
        return first_tickets + \
                (additional_tickets['seating_adult__sum'] or 0) + \
                (additional_tickets['seating_child__sum'] or 0)

    reservations_made.short_description = "Забронировано билетов"
    reservations_made = property(reservations_made)

    def get_absolute_url(self):
        return reverse(
                'booking_show',
                kwargs={'show_pk': self.pk},
                )

    def __str__(self):
        return ("Показ спектакля %s на площадке %s, время проведения: %s"  %
                (self.performance.title, self.place.title,
                    print_local_datetime(self.showtime)))

    def save(self, *args, **kwargs):
        if self.price is None:
            self.price = self.place.default_price
        if self.price_adult is None:
            self.price_adult = self.place.default_price_adult or \
                self.place.default_price
        if self.price_child is None:
            self.price_child = self.place.default_price_child or \
                self.place.default_price
        super(Schedule, self).save(*args, **kwargs)

    class Meta:
        ordering=["showtime"]
        get_latest_by = "showtime"
        verbose_name = "Расписание"
        verbose_name_plural = "Расписание (управление)"

@python_2_unicode_compatible
class Promo(models.Model):
    """
    Откуда узнали
    - Источник информации
    """
    source = models.CharField("Источник информации", max_length=256,
            help_text="Например: сарафанное радио, или реклама в блоге...")

    def __str__(self):
        return "Тип источника информации: %s" % self.source

    class Meta:
        verbose_name = "Источник информации"
        verbose_name_plural = "Источники"


@python_2_unicode_compatible
class Reservation(models.Model):
    """
    Бронь
    - Зритель -> Spectator*
    - Дата бронирования (авто)
    - Показ
    - Дополнительно взрослых мест* (0)
    - Дополнительно детских мест* (0)
    """
    spectator = models.ForeignKey(Spectator, verbose_name="Зритель")
    reservation_date = models.DateTimeField("Дата бронирования", auto_now_add=True)
    show = models.ForeignKey(Schedule, verbose_name="Показ",
            related_name="reservations")
    seating_adult = models.PositiveSmallIntegerField("Дополнительные места (взр)",
            default=0)
    seating_child = models.PositiveSmallIntegerField("Дополнительные места (детск)",
            default=0)
    promo_source = models.ForeignKey(Promo, blank=True, null=True)
    settled = models.BooleanField("Бронь выкуплена", default=False)
    cancelled = models.BooleanField("Бронь отменена", default=False,
            help_text="Установите галочку, чтобы отменить просроченную бронь"
            "и отправить автоматическое уведомление",
            )
    additional_info = models.TextField("Комментарии к бронированию",
            blank=True,
            help_text="Дополнительные сведения, пожелания...")

    def expired(self):
        return (not self.settled) and timezone.now() >= self.expires
    expired.short_description = "Бронь просрочена"
    expired.boolean = True
    expired = property(expired)

    def expires(self):
        return self.show.showtime - timedelta(
                settings.RESERVATIONS_EXPIRES_BEFORE
                )
    expires.short_description = "Выкуп брони до"
    expires = property(expires)

    def reference_number(self):
        return 1000000 + int(self.pk)
    reference_number.short_description = "Номер брони"
    reference_number = property(reference_number)

    def total_cost(self):
        return (
            self.show.price + self.show.price_adult * self.seating_adult +
            self.show.price_child * self.seating_child
        )
    total_cost.short_description = "Общая стоимость билетов"
    total_cost = property(total_cost)


    def __str__(self):
        return ("Бронь на показ спектакля %s, на площадке %s %s" % (
                self.show.performance.title,
                self.show.place.title,
                print_local_datetime(self.show.showtime),
                ))

    class Meta:
        ordering=["-reservation_date"]
        get_latest_by = "reservation_date"
        verbose_name = "Бронь"
        verbose_name_plural = "Брони"


@python_2_unicode_compatible
class Review(models.Model):
    """
    Отзывы
    - Зритель
    - (email)
    - Спектакль -> Performance
    - Площадка -> Place
    - Отзыв
    """
    review_date = models.DateField("Дата отзыва")
    spectator = models.ForeignKey(Spectator, verbose_name="Зритель")
    performance = models.ForeignKey(Performance, blank=True, null=True,
            related_name="reviews", verbose_name="Спектакль")
    place = models.ForeignKey(Place, blank=True, null=True,
            verbose_name="Площадка",
            related_name="reviews")
    text = models.TextField("Отзыв")

    def __str__(self):
        return "Отзыв от зрителя %s" % self.spectator.name

    class Meta:
        ordering=["-review_date"]
        get_latest_by = "review_date"
        verbose_name = "Отзыв"
        verbose_name_plural = "Отзывы"


#
# Here go CMS stuff
#


from cms.models import CMSPlugin

@python_2_unicode_compatible
class ReservationPlugin(CMSPlugin):
    performance = models.ForeignKey(Performance,
            verbose_name="Спектакль",
            help_text="Здесь можно будет забронировать билеты на данный "
            "спектакль",
            blank=True, null=True)

    place = models.ForeignKey(Place,
            verbose_name="Площадка",
            help_text="Здесь можно будет забронировать билеты на данную "
            "площадку",
            blank=True, null=True)


    def __str__(self):
        return "Бронирование билетов на спектакль %s на площадке %s" % (
                (self.performance.title if self.performance else "----"),
                (self.place.title if self.place else "----"),
                )



@python_2_unicode_compatible
class SchedulePlugin(CMSPlugin):
    number = models.PositiveSmallIntegerField(
            "Количество блоков",
            default=12,
            help_text="Сколько блоков будет показано на странице",
            )

    def __str__(self):
        return "Превью ближайших %s показов" % self.number

#=======================================
# Signals
#=======================================


from django.db.models.signals import pre_save
from django.dispatch import receiver
from reservations.comm import (PaymentConfirmationMessage,
        CancellingMessage)

@receiver(pre_save, sender=Reservation, dispatch_uid="send-payment-received")
def send_payment_received(sender, **kwargs):
    log.debug("sending payment received")
    instance = kwargs['instance']
    try:
        db_settled = sender.objects.get(pk=instance.pk).settled
    except sender.DoesNotExist:
        return

    log.debug("instance is %s, db_settled is %s, instance.settled is %s",
            instance, db_settled, instance.settled)
    if instance.settled and not db_settled:
        tmpl = instance.show.place.payment_received_body
        log.debug("tmpl is %s", tmpl)
        confirmation = PaymentConfirmationMessage(
                body_template = tmpl,
                to = [instance.spectator.email],
                spectator = instance.spectator,
                place = instance.show.place,
                show = instance.show,
                performance = instance.show.performance,
                manager = instance.show.place.manager,
                )
        confirmation.send()

@receiver(pre_save, sender=Reservation, dispatch_uid="send-cancelling")
def send_cancelling(sender, **kwargs):
    instance = kwargs['instance']
    try:
        db_cancelled = sender.objects.get(pk=instance.pk).cancelled
    except sender.DoesNotExist:
        return
    if instance.cancelled and not db_cancelled:
        tmpl = instance.show.place.cancelling_body
        log.debug("tmpl is %s", tmpl)
        msg = CancellingMessage(
                body_template = tmpl,
                to = [instance.spectator.email],
                show = instance.show,
                manager = instance.show.place.manager,
                spectator = instance.spectator,
                )
        msg.send()
