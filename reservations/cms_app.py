# -*- coding: utf-8 -*-
from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool


class ReservationsApp(CMSApp):
    name = u"Афиша и бронирование" 
    urls = ['reservations.urls']
    app_name = 'reservations'

apphook_pool.register(ReservationsApp)
