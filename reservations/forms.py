#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from logging import getLogger
log = getLogger('reservations.forms')

from django import forms
from reservations import models as m
from reservations.utils import print_local_datetime



class ShowChooser(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "%s (стоимость: %s р.)" % (
                print_local_datetime(obj.showtime),
                obj.price
                )



class BookingForm(forms.Form):
    _spectator_fields = (
            "email",
            "first_name",
            "last_name",
            "tel",
            "children",
            )

    email = forms.EmailField(
            label="email",
            required=True,
            max_length=256,
            )

    first_name = forms.CharField(
            label="Имя",
            max_length=50,
            required=False,
            )

    last_name = forms.CharField(
            label="Фамилия",
            max_length=50,
            required=False,
            )

    tel = forms.CharField(
            label="Мобильный телефон",
            required=True,
            )

    children = forms.CharField(
            label="Сведения о детях",
            widget=forms.Textarea,
            required=False,
            help_text = "Имена и возраст детей",
            )


    show = ShowChooser(
            label="Дата показа",
            queryset = m.Schedule.available.all(),
            empty_label = "Выберите дату",
            help_text = "Выберите дату и время показа",
            )

    seating_adult = forms.IntegerField(
            label="Количество билетов (взр)",
            initial=1, min_value=1, max_value=2
            )

    seating_child = forms.IntegerField(
            label="Количество билетов (детск)",
            initial=1, min_value=1, max_value=2
            )

    additional_info = forms.CharField(
            label="Дополнительные сведения",
            widget=forms.Textarea,
            required=False,
            )

    def remove_credentials(self):
        for fname in self._spectator_fields:
            self.fields.pop(fname, None)
        return self

    def set_show_initial(self, show_id):
        if show_id is not None:
            self.fields['show'].initial = show_id
        return self

    def set_show_qs(self, show_qs):
        self.fields['show'].queryset = show_qs
        return self




class PreOrderForm(forms.Form):
    performance = forms.ModelChoiceField(
            queryset=m.Performance.objects.filter(
                pk__in=m.Schedule.available.all().values_list(
                    'performance', flat=True)
            ),
            label="Выберите спектакль",
            empty_label="Выберите спектакль",
            initial=0
            )

    place = forms.ModelChoiceField(
            queryset=m.Place.objects.filter(
                pk__in=m.Schedule.available.all().values_list(
                    'place', flat=True)
            ),
            label="Выберите площадку",
            empty_label="Выберите площадку",
            initial=0
            )

    email = forms.EmailField(
            required=False,
            )


    def set_field(self, field_name, initial=None):
        self.fields[field_name] = forms.CharField(
                    widget=forms.HiddenInput,
                    initial=initial,
                    )
        return self

    def set_place(self, place_pk):
        # Select only perfomances shown in this place
        perf_qs = m.Performance.objects.filter(
                pk__in=m.Schedule.available.filter(
                    place__pk=place_pk
                    ).values_list('performance', flat=True),
                )
        if perf_qs.exists():
            self.fields['performance'].queryset = perf_qs
            return self.set_field("place", place_pk)

    def set_performance(self, performance_pk):
        # Select only places where this performance is shown
        place_qs = m.Place.objects.filter(
                pk__in=m.Schedule.available.filter(
                    performance__pk=performance_pk
                    ).values_list('place', flat=True),
                )
        if place_qs.exists():
            self.fields['place'].queryset = place_qs
            return self.set_field("performance", performance_pk)

    def set_spectator(self, spectator):
        if spectator is not None:
            self.fields.pop("email", None)
        return self
