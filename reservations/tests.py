#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from logging import getLogger
log = getLogger("reservations.tests")

from locale import LC_ALL, setlocale, format as l_fmt

from random import randint

from django.test import TestCase
from datetime import timedelta
from django.utils import timezone
t1 = timezone.localtime(timezone.now(), timezone.get_default_timezone())
tpast = t1 - timedelta(1)
tfuture = t1 + timedelta(1)
from datetime import time
times = (time(11), time(13), time(18,30))

# Create your tests here.


from django.utils.importlib import import_module
from django.contrib.auth.models import User
from django.conf import settings
from django.core import mail

from reservations import models as m

setlocale(LC_ALL, locale=b"ru_RU.UTF8")
user_pass = "123321"

spectator_form_fieldnames = (
        "email",
        "tel",
        "first_name",
        "last_name",
        "children"
        )


class ReservationsTestBase(TestCase):
    fixtures = ["reservations_init.json"]
    url1 = "/playbill/booking/1/1/"
    thank_you_url = "/playbill/booking/thank-you/"
    session_key = "SPECTATOR_SESSION"
    manager_tel = "123321"
    manager_email = "olga@mail.ru"

    def setUp(self):
        self.reg_spect = self.registered_spectator()
        self.unreg_spect= self.unregistered_spectator()
        # initialize manager for 1st place
        place = m.Place.objects.get(pk=1)
        user = User.objects.create_user(
                    'olga', self.manager_email, '123321', 
                    first_name='Olga',
                    last_name='Ustugova',
                    )
        manager = m.PlaceManager(tel=self.manager_tel, user=user)
        manager.save()
        place.manager = manager
        place.how_to_get = "how to get"
        place.pay_instructions = "pay instructions"
        place.ticket_instructions = "ticket instructions"
        place.save()
        self.show = m.Schedule.objects.create(
                performance = m.Performance.objects.get(pk=1),
                place = place,
                showtime = timezone.now() + timedelta(1),
                )
        settings.SESSION_ENGINE = 'django.contrib.sessions.backends.file'
        engine = import_module(settings.SESSION_ENGINE)
        store = engine.SessionStore()
        store.save()
        self.session = store
        self.client.cookies[settings.SESSION_COOKIE_NAME] = store.session_key

    def reinitialize_session(self):
        # http://code.djangoproject.com/ticket/10899
        self.session.flush()
        self.client.cookies[settings.SESSION_COOKIE_NAME] = self.session.session_key

    def registered_spectator(self):
        email="registered@mail.ru"
        spectator = m.Spectator(
                email=email,
                first_name="maxim",
                tel="123321",
                children="children",
                )
        user = User.objects.create_user('maxim_user', email, user_pass)
        user.save()
        spectator.user = user
        spectator.save()
        return spectator
        

    def unregistered_spectator(self):
        email="unregistered@mail.ru"
        spectator = m.Spectator(
                email=email,
                first_name="Alex",
                tel="123321",
                children="children",
                )
        spectator.save()
        return spectator


    def fill_shows_randomly(self):
        place1 = m.Place.objects.get(pk=1)
        place2 = m.Place.objects.get(pk=2)
        performance1 = m.Performance.objects.get(pk=1)
        performance2 = m.Performance.objects.get(pk=2)

        count = 0
        for d in range(0, 30, 5):
            for t in times:
                count += 1
                place = (place1, place2, place2, place1, place1)[count % 5]
                perf = (performance2, performance1, performance2)[count % 3]
                m.Schedule(
                        showtime=tpast.replace(
                            hour=t.hour, minute=t.minute, second=0
                            ) + timedelta(d),
                        place = place,
                        performance=perf).save() 


    def fill_shows(self, perf_pk, place_pk, amount):
        place = m.Place.objects.get(pk=place_pk)
        perf = m.Performance.objects.get(pk=perf_pk)

        count = 0
        for d in range(0, 30, randint(1,4)):
            t = times[randint(0, 2)]
            if count == amount: return
            count += 1
            m.Schedule(
                    showtime=tfuture.replace(
                        hour=t.hour, minute=t.minute, second=0
                        ) + timedelta(d),
                    place = place,
                    performance=perf).save() 


class BookingViewSpectatorTests(ReservationsTestBase):
    """
    Test booking:
    - new and unregistered users:
        +should see full clean form
        +post with new email should create new spectator
        +post with unregistered email should update existing spectator
    - registered users:
        +should not see credentials fields
    - all users:
        + previously supplied email should be in initial value
    """


    def test_new_spectator(self):
        """
        +should see full clean form
        """
        self.reinitialize_session()

        response = self.client.get(self.url1)
        self.assertEqual(response.status_code, 200)
        self.assertIn("form", response.context_data)
        form = response.context_data['form']

        # all fields present
        for fname in spectator_form_fieldnames:
            self.assertIn(fname, form.fields)

            # make sure there's no initials
            self.assertNotIn(fname, form.initial)
            self.assertIs(form.fields[fname].initial, None)


        # there are no hidden fields
        self.assertEqual(len(form.hidden_fields()), 0)

    def test_unreg_spect(self):
        """
        set session marker for unregistered user
        should still show empty default form
        only email should be provided:
        + previously supplied email should be in initial value
        """
        spect_session = {}
        spect_session["spectator_id"] = self.unreg_spect.id
        session = self.session
        session[self.session_key] = spect_session
        session.save()
        
        response = self.client.get(self.url1)
        form = response.context_data['form']


        # make sure there's no initials but in email
        for fname in spectator_form_fieldnames:
            field = form.fields[fname]
            if fname == "email":
                self.assertEqual(
                        field.initial,
                        self.unreg_spect.email
                        )
            else:
                self.assertIs(field.initial, None)
                self.assertNotIn(fname, form.initial)


        # email should not be hidden
        self.assertNotIn("email", form.hidden_fields())

    def test_new_spectator_create(self):
        """
        +post with new email should create new spectator
        """

        new_email = "newunregistered@mail.ru"
        response = self.client.post(
                self.url1,
                data = {
                    "email": new_email,
                    "seating": 1,
                    "show": self.show.pk,
                    },
                )
        log.debug("There are %s shows scheduled", len(m.Schedule.objects.all()))
        log.debug("response is %s", unicode(response.content, encoding="utf-8"))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(
                len(m.Spectator.objects.filter(
                    email = new_email 
                    )),
                1)

    def test_unreg_spect_update(self):
        """
        +post with unregistered email should update existing spectator
        """
        updated = dict(
                first_name = "Alex updated",
                last_name = "Last name",
                tel = "123321",
                children = "Lorem ipsum",
                email = self.unreg_spect.email,
                )
        data = updated.copy()
        data.update({
                    "seating": 1,
                    "show": self.show.pk,
                    })

        response = self.client.post(
                self.url1,
                data = data,
                )
        self.assertEqual(response.status_code, 302)
        #check for the updated fields
        for fname in updated:
            updated_unreg = m.Spectator.objects.get(
                    pk = self.unreg_spect.pk)
            self.assertEqual(
                    m.Spectator._meta.get_field(fname).value_from_object(
                        updated_unreg),
                    updated[fname]
                    )

    def test_reg_spect(self):
        """
        +should not see credentials fields
        """

        # manipulate session to make it know current spectator
        spect_session = {}
        spect_session["spectator_id"] = self.reg_spect.id
        session = self.session
        session[self.session_key] = spect_session
        session.save()
        
        response = self.client.get(self.url1)
        form = response.context_data['form']

        for fname in spectator_form_fieldnames:
            self.assertNotIn(fname, form.fields)
    
    def test_authenticated_spect(self):
        """
        +should not see credentials fields
        """
        self.reinitialize_session()
        authenticated = self.client.login(
                username="maxim_user",
                password=user_pass
                )

        self.assertTrue(authenticated)
        response = self.client.get(self.url1)
        form = response.context_data['form']

        for fname in spectator_form_fieldnames:
            self.assertNotIn(fname, form.fields)


class BookingViewReservationTest(ReservationsTestBase):
    """
    - new reservation should be created and stored in session
    - required fields:
        + email
        + seating (> 0)
        + show
    """
    def make_post(self, email):
        return self.client.post(
                self.url1,
                data = {
                    "email": email,
                    "seating": 1,
                    "show": self.show.pk,
                    },
                )

    def test_reservation_create(self):
        """
        - new reservation should be created and stored in session
        """
        new_email = "newunregistered@mail.ru"
        self.make_post(new_email)
        self.reinitialize_session()
        self.make_post(self.unreg_spect.email)
        self.reinitialize_session()
        self.make_post(self.reg_spect.email)
        
        # Checking
        for email in (
                new_email,
                self.unreg_spect.email,
                self.reg_spect.email
                ):
            self.assertTrue(
                m.Reservation.objects.filter(
                    spectator__email=email).exists()
                )


class BookingViewScheduleTest(ReservationsTestBase):
    """
    - showtimes must conform to the choosen combination of perf and place
    - sold_out shows should not be provided
    - if there are no available shows for the choosen combination, 
    than "booking not available" message should appear
    """
    url12 = '/playbill/booking/1/2/'
    url22 = '/playbill/booking/2/2/'
    url21 = '/playbill/booking/2/1/'
    amount11 = 4
    amount12 = 7
    place1 = m.Place.objects.get(pk=1)
    perf1 = m.Performance.objects.get(pk=1)


    def test_schedule_match(self):
        """
        - showtimes must conform to the choosen combination of perf and place
        """
        # cleanup first
        for s in m.Schedule.complete_object_list.all():
            s.delete()

        # create
        self.fill_shows(1, 1, self.amount11)
        self.fill_shows(1, 2, self.amount12)
        
        show22 = m.Schedule(
                showtime = tfuture,
                place = m.Place.objects.get(pk=2),
                performance = m.Performance.objects.get(pk=2),
                )
        show22.save()

        # check amounts
        response11 = self.client.get(self.url1)
        response12 = self.client.get(self.url12)
        response22 = self.client.get(self.url22)
        self.assertEqual(response11.status_code, 200)
        self.assertEqual(response12.status_code, 200)
        self.assertEqual(response22.status_code, 200)
        self.assertEqual(
                response11.context['form'].fields['show'].queryset.count(),
                self.amount11
                )
        self.assertEqual(
                response12.context['form'].fields['show'].queryset.count(),
                self.amount12
                )
        self.assertEqual(
                response22.context['form'].fields['show'].queryset.count(),
                1
                )

        # check that the only 22 showtime is right
        self.assertEqual(
                response22.context['form'].fields['show'].queryset[0].showtime,
                tfuture
                )
    
    def test_schedule_sold(self):
        """
        - sold_out shows should not be provided
        """
        # cleanup first
        for s in m.Schedule.complete_object_list.all():
            s.delete()

        show_sold = m.Schedule(
                showtime = tfuture,
                performance = self.perf1,
                place = self.place1
                )
        show_sold.sold_out = True
        show_sold.save()
        available_time = tfuture + timedelta(11)
        show_available = m.Schedule(
                showtime = available_time,
                performance = self.perf1,
                place = self.place1
                )
        show_available.save()

        resp = self.client.get(self.url1)
        for show in resp.context['form'].fields['show'].queryset:
            self.assertEqual(show.showtime, available_time)
    
    def test_schedule_empty(self):
        """
        - if there are no available shows for the choosen combination, 
        than "booking not available" message should appear
        """

        for s in m.Schedule.complete_object_list.all():
            s.delete()

        # initialize only not availavle shows
        m.Schedule(
                showtime = tpast,
                performance = self.perf1,
                place = self.place1
                ).save()

        show_sold = m.Schedule(
                showtime = tfuture,
                performance = self.perf1,
                place = self.place1
                )
        show_sold.sold_out = True
        show_sold.save()
         
        resp = self.client.get(self.url1)
        self.assertIs(
                resp.context["form"], 
                None
                )


class ThankYouSpectatorTest(ReservationsTestBase):
    """
    ThankYou view spectator data test

    authenticated
    - should see full details in confirmation
    - should see link to profile

    registered (not authenticated)
    - should not see credentials in confirmation
    - should see email that will be dispatched with confirmation
    - should be prompted to login to control personal data

    unregistered
    - should see only credentials from current session
    - should not see any credentials from previously saved data in model
    if they are not in the current session
    - should be prompted to register

    unknown
    - should receive 404
    """

    def test_unknown_spectator(self):
        """
            - should receive 404
        """
        self.client.logout()
        self.reinitialize_session()
        resp = self.client.get(self.thank_you_url)
        self.assertEqual(resp.status_code, 404)

    def test_unregistered_session_credentials(self):
        """
        - should see only credentials from current session
        - should not see any credentials from previously saved data in model
        if they are not in the current session
        """

        self.client.logout()
        self.reinitialize_session()
        
        # manipulate session to inject some credentials
        spectator_session = dict(
                spectator_data = dict(
                    last_name="You should see this",  # last_name in session, first - in model
                    tel="12345",
                    children="Information about children",
                    email=self.unreg_spect.email,
                    ),
                )
        session = self.session
        session[self.session_key] = spectator_session
        session.save()

        resp = self.client.get(self.thank_you_url)
        for fname in spectator_session['spectator_data']:
            self.assertContains(
                    resp,
                    spectator_session['spectator_data'][fname],
                    status_code=200,
                    )
        self.assertNotContains(
                resp,
                self.unreg_spect.first_name,
                )

    def test_unregistered_register_prompt(self):
        """
        - should be prompted to register
        """
        pass

    def test_registered_credentials(self):
        """
        - should not see credentials in confirmation
        - should see email
        """
        self.client.logout()
        self.reinitialize_session()

        # manipulate session to set registered user
        spectator_session = dict(
                spectator_id=self.reg_spect.id,
                )
        session = self.session
        session[self.session_key] = spectator_session
        session.save()

        resp = self.client.get(self.thank_you_url)
        for fname in spectator_form_fieldnames:
            if fname != "email":
                value = str(m.Spectator._meta.get_field(
                    fname).value_from_object(self.reg_spect)).strip()
                if value:
                    self.assertNotContains(resp, value)
        self.assertContains(resp, self.reg_spect.email)

    def test_registered_login_prompt(self):
        """
        - should be prompted to login to control personal data
        """
        pass

    def test_authenticated_credentials(self):
        """
        - should see full details in confirmation
        """
        self.reinitialize_session()
        authenticated = self.client.login(
                username="maxim_user",
                password=user_pass
                )
        self.assertTrue(authenticated)
        
        resp = self.client.get(self.thank_you_url)
        for fname in spectator_form_fieldnames:
            self.assertContains(
                        resp,
                        m.Spectator._meta.get_field(fname).value_from_object(
                            self.reg_spect),
                        )

    def test_authenticated_profile_link(self):
        """
        - should see link to profile
        """
        pass

class EmailMessageTest(ReservationsTestBase):
    """
    Email dispatch testing:
    - Confirmations should be sent after each new reservation
        + subject must contain performance.title - place.title
        + sum should be equal to seating * price
        + first_name should appear in body
        + reference_number should correspond to the last reservation by this spectator
        + manager tel should be correct 
    """

    cancelling_subject = "Отмена брони"
    payment_received_subject = "Подтверждение платежа"


    def clean_outbox(self):
        mail.outbox = []

    def confirmation_testing(self, spectator):
        log.debug("Place: %s, manager: %s", self.show.place.title, self.show.place.manager.email)
        self.reinitialize_session()
        self.clean_outbox()
        seating_num = 3
        perf_title = m.Performance.objects.get(pk=1).title.upper()
        place_title = m.Place.objects.get(pk=1).title
        resp = self.client.post(
                self.url1,
                data = {
                    "email": spectator.email,
                    "first_name": spectator.first_name,
                    "seating": seating_num,
                    "show": self.show.pk,
                    },
                )
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(len(mail.outbox), 2)
        self.assertIn(mail.outbox[0].to[0], spectator.email)
        self.assertIn(perf_title, mail.outbox[0].subject)
        self.assertIn(place_title, mail.outbox[0].subject)
        amount = seating_num * self.show.price
        body = mail.outbox[0].body
        self.assertIn(l_fmt("%.1f", amount), body)
        self.assertIn(spectator.first_name, body)
        reference_number = m.Reservation.objects.last().reference_number
        self.assertIn(str(reference_number), body)
        self.assertIn(self.show.place.manager.tel, body)
        self.assertIn(self.show.place.pay_instructions, body)

    def test_confirmation_sent_unregistered(self):
        # make reservation by unregistered user
        spectator = self.unreg_spect
        self.confirmation_testing(spectator)

    def test_confirmation_sent_registered(self):
        # make reservation by registered user
        spectator = self.reg_spect
        self.confirmation_testing(spectator)

    def notification_testing(self, spectator):
        self.reinitialize_session()
        self.clean_outbox()
        seating_num = 3
        comments = "blah blah blah"
        resp = self.client.post(
                self.url1,
                data = {
                    "email": spectator.email,
                    "first_name": spectator.first_name,
                    "seating": seating_num,
                    "show": self.show.pk,
                    "additional_info": comments,
                    },
                )
        # ======================================== 
        self.assertEqual(len(mail.outbox), 2)
        body = mail.outbox[1].body
        self.assertEqual(resp.status_code, 302)
        self.assertEqual(self.manager_email, mail.outbox[1].to[0])
        self.assertIn(spectator.first_name, body)
        self.assertIn(spectator.tel, body)
        self.assertIn(spectator.email, body)
        self.assertIn(str(seating_num), body)
        self.assertIn(comments, body)

    def test_notification_sent_unregistered(self):
        self.notification_testing(self.unreg_spect)

    def test_notification_sent_registered(self):
        self.notification_testing(self.reg_spect)

    def cancelling_testing(self, spectator):
        self.reinitialize_session()
        reservation = m.Reservation.objects.create(
                show=self.show,
                spectator=spectator,
                seating=2,
                )
        self.clean_outbox()
        reservation.cancelled = True
        reservation.save()

        self.assertEqual(len(mail.outbox), 1)
        body = mail.outbox[0].body
        self.assertEqual(mail.outbox[0].to[0], spectator.email)
        self.assertEqual(self.cancelling_subject, mail.outbox[0].subject)
        self.assertIn(self.show.place.manager.tel, body)
        self.assertIn(spectator.first_name, body)

    def test_cancelling_sent_unregistered(self):
        self.cancelling_testing(self.unreg_spect)

    def test_cancelling_sent_registered(self):
        self.cancelling_testing(self.reg_spect)

    def payment_received_testing(self, spectator):
        self.reinitialize_session()
        reservation = m.Reservation.objects.create(
                show=self.show,
                spectator=spectator,
                seating=2,
                )
        self.clean_outbox()
        reservation.settled = True

        reservation.save()

        #======================================== 
        self.assertEqual(len(mail.outbox), 1)
        body = mail.outbox[0].body
        self.assertEqual(mail.outbox[0].to[0], spectator.email)
        self.assertEqual(self.payment_received_subject, mail.outbox[0].subject)
        self.assertIn(self.show.place.manager.tel, body)
        self.assertIn(spectator.first_name, body)
        self.assertIn(self.show.place.tickets_instructions, body)
        self.assertIn(self.show.place.how_to_get, body)
        self.assertIn(self.show.performance.brief, body)
        self.assertIn(self.show.place.brief, body)
        self.assertIn(self.show.place.title, body)

    def test_payment_sent_unregistered(self):
        self.payment_received_testing(self.unreg_spect)

    def test_payment_sent_registered(self):
        self.payment_received_testing(self.reg_spect)
