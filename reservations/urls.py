from django.conf.urls import *
from django.contrib.auth.decorators import login_required

from reservations import views

# root = /reservations/
urlpatterns = patterns('',
        # /reservations/
        url(r'^$', views.ScheduleView.as_view(),
            name='index'),
        # /reservations/places/
        url(r'^places/$', views.PlaceListView.as_view(),
            name='place_list'),
        # /reservations/places/1/
        url(r'^places/(?P<pk>\d+)/$', views.PlaceView.as_view(),
            name='places'),
        # /reservations/performances/1/
        url(r'^performances/(?P<pk>\d+)/$', views.PerformanceView.as_view(),
            name='performances'),
        # /reservations/booking/
        url(r'^booking/$', views.BookingPreorderView.as_view(),
            name='booking_preorder'),
        # /reservations/booking/1/2/
        url(r'^booking/(?P<performance_pk>\d+)/(?P<place_pk>\d+)/$',
            views.BookingView.as_view(), name='booking'),
        # /reservations/booking/api/(performances|places|schedules)/?performance=1&?place=2
        url(r'^booking/api/(?P<dataset>\w+)/$',
            views.BookingAPIView.as_view(), name='booking_api'),
        # /reservations/booking/show/3
        url(r'^booking/show/(?P<show_pk>\d+)$',
            views.BookingView.as_view(), name='booking_show'),
        url(r'^booking/thank-you/$', views.ThankYouView.as_view(),
            name='thank_you'),
        url(r'^spectator/$',
            login_required(views.SpectatorProfileView.as_view()),
            name='profile'),
        url(r'^spectator/update/$',
            login_required(views.SpectatorUpdateView.as_view()),
            name='update_profile'),
        url(r'^reservation/cancel/(?P<pk>\d+)/$',
            login_required(views.ReservationCancelView.as_view()),
            name='cancel_reservation'),
        )
