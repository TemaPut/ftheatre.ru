#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals 

from django.utils import timezone
from django.utils.dateformat import format as format_date

print_local_datetime = lambda dt: format_date(
        dt.astimezone(timezone.get_current_timezone()),
        "j E H:i")
