#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from logging import getLogger
log = getLogger("reservations.comm")
from smtplib import  SMTPDataError


from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives, mail_admins
from django.template import loader, TemplateDoesNotExist, Context

class BaseMessage(dict):

    def __init__(self, subject_template, body_template):
        self.subject_template = subject_template
        self.body_template = body_template
        return super(BaseMessage, self).__init__()

    def compile_message(self, context_dict=None, body_type="body"):
        self.compile_subject(self.subject_template, context_dict)
        self.compile_body(self.body_template, context_dict, body_type)
        return self

    def compile_subject(self, subject_template, context_dict=None):
        self["subject"] = loader.render_to_string(
                subject_template, context_dict).replace("\n", "")

    def compile_body(self, template_name, context_dict=None, body_type="body"):
        self[body_type] = loader.render_to_string(template_name, context_dict)


class CustomTemplateMessage(BaseMessage):

    def compile_body(self, template, context_dict=None, body_type="body"):
        """
        Gets template as string
        """
        ctx = Context(context_dict)
        self[body_type] = loader.get_template_from_string(
            template, context_dict).render(ctx)



class BaseCommunicator(object):
    sending_methods = (
            'send_email',
            )
    additional_templates = []

    def __init__(self, **kwargs):
        self.context = {}
        self.data = {}
        self.data = dict(
                to = kwargs.pop("to", None),
                )
        self.context.update(kwargs)

    def prepare_context(self):
        return self.context

    def send(self):
        message = self.compile_message()
        for method in self.sending_methods:
            getattr(self, method, 'noop')(message)

    def noop(self, message):
        pass

    def send_email(self, message):
        if self.data['to']:

            if "html" in message:
                email = EmailMultiAlternatives(
                        message["subject"],
                        message["body"],
                        None,
                        self.data['to'],
                        )
                email.attach_alternative(
                        message["html"],
                        "text/html",
                        )
            else:
                email = EmailMessage(
                        message["subject"],
                        message["body"],
                        None,
                        self.data['to'],
                        )
            try:
                email.send()
            except SMTPDataError as e:
                log.error("SMTP error %s\n"
                          "Message was for %s with subject %s",
                           e, email.to, email.subject)
                try:
                    mail_admins("SMTP error on firsttheatre!!!")
                except:
                    pass


    def compile_message(self):
        context = self.prepare_context()
        message = self.get_message(context)

        for template_name, body_type in self.additional_templates:
            try:
                message.compile_body(template_name, context, body_type)
            except TemplateDoesNotExist:
                log.warning("Template not found %s", template_name)
        return message

    def get_message(self, context):
        return BaseMessage(
            self.subject_template,
            self.body_template
        ).compile_message(context)


class CustomTemplateCommunicator(BaseCommunicator):
    """
    Gets template from sepcified model field
    """

    def __init__(self, **kwargs):
        body_template = kwargs.pop("body_template", "")
        self.custom_body_template = body_template.strip() or None

        return super(CustomTemplateCommunicator, self).__init__(**kwargs)

    def get_message(self, context):
        if self.custom_body_template is None:
            return super(CustomTemplateCommunicator, self).get_message(context)
        return CustomTemplateMessage(
            self.subject_template,
            self.custom_body_template
        ).compile_message(context)




class ConfirmationMessage(CustomTemplateCommunicator):
    subject_template = "reservations/emails/confirmation_subject.txt"
    body_template = "reservations/emails/confirmation_plain_body.txt"

class NotificationMessage(BaseCommunicator):
    subject_template = "reservations/emails/notification_subject.txt"
    body_template = "reservations/emails/notification_plain_body.txt"

class PaymentConfirmationMessage(CustomTemplateCommunicator):
    subject_template = "reservations/emails/payment_received_subject.txt"
    body_template = "reservations/emails/payment_received_plain_body.txt"

class CancellingMessage(CustomTemplateCommunicator):
    subject_template = "reservations/emails/cancelling_subject.txt"
    body_template = "reservations/emails/cancelling_plain_body.txt"
