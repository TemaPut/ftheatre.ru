#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals 

from logging import getLogger
log = getLogger("reservations.utils")

from django.utils.encoding import python_2_unicode_compatible
from django.utils import timezone
from django.db.models.loading import get_model

print_local_datetime = lambda dt: dt.astimezone(timezone.get_default_timezone()).strftime("%d %h %H:%M")

Spectator = get_model("reservations","Spectator")
Reservation = get_model("reservations", "Reservation")

@python_2_unicode_compatible
class SpectatorSession(object):
    """
    Holds information about current spectator in session
    """

    key = "SPECTATOR_SESSION"
    spectator_fields = (
            "email",
            "first_name",
            "last_name",
            "tel",
            "children",
            )

    def __init__(self, request, data=None):
        if self.key not in request.session:
            request.session[self.key] = {}

        self._cache = {}
        self.request = request
        self.session = request.session[self.key]
        self.spectator_data = self.get_spect_data(data)

        self.update_session()

    
    def get_spect_data(self, data):
        spectator_data = self.session.setdefault("spectator_data", {})
        if data is not None:
            spectator_data.update(
                {key:data[key] for key in data if key in self.spectator_fields}
                )
        return spectator_data


    @property
    def spectator(self):
        """
        Try to find spectator with user (if exists) or email
        """
        spectator = None

        if "spectator" in self._cache:
            return self._cache['spectator']
        if self.request.user.is_authenticated() and hasattr(
                self.request.user, "spectator"):
            spectator = self.get_spectator_from_user()
        if spectator is None:
            if "spectator_id" in self.session:
                try:
                    spectator = Spectator.objects.get(
                        id=self.session["spectator_id"]
                        )
                except Spectator.DoesNotExist: pass
        if spectator is None:
                spectator = self.get_spectator_by_email()
        if spectator is not None:
            self._cache['spectator'] = spectator
            return spectator

    @property
    def user(self):
        """
        Try to find user from spectator if it is not authenticated
        """
        user = None
        if "user" in self._cache:
            return self._cache['user']
        if self.request.user.is_authenticated():
            if hasattr(self.request.user, "spectator"):
                user = self.request.user
        if user is None:
            user = self.get_user_from_spectator()
        if user is not None:
            self._cache["user"] = user
            return user

    @property
    def reservation(self):
        if "reservation_id" in self.session:
            try:
                return Reservation.objects.get(
                        id=self.session["reservation_id"]
                        )
            except Reservation.DoesNotExist: pass

    def get_user_from_spectator(self):
        if self.spectator is not None:
            return self.spectator.user

    def get_spectator_from_user(self):
        if self.user is not None:
            try:
                return self.user.spectator
            except Spectator.DoesNotExist:
                pass

    def get_spectator_by_email(self):
        if "email" in self.spectator_data:
            try:
                return Spectator.objects.get(
                    email=self.spectator_data["email"]
                    )
            except Spectator.DoesNotExist:
                pass

    def set_reservation(self, reservation):
        self.session["reservation_id"] = reservation.id
        self.request.session.modified = True

    def set_spectator(self, spectator):
        self.session["spectator_id"] = spectator.id
        self.request.session.modified = True

    def update_session(self):
        self.session['spectator_data'] = self.spectator_data
        self.request.session.modified = True

    def __str__(self):
        return self.request.session[self.key].__str__()
