#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from reservations.models import (Schedule, Place, Performance,
        PerformanceProxy, Reservation, PlaceManager)

def create_modeladmin(modeladmin, model, name = None):
    class  Meta:
        proxy = True
        app_label = model._meta.app_label
        verbose_name = model._meta.verbose_name
        verbose_name_plural = model._meta.verbose_name_plural

    attrs = {'__module__': '', 'Meta': Meta}

    newmodel = type(bytes(name), (model,), attrs)

    admin.site.register(newmodel, modeladmin)
    return modeladmin


class ScheduleInline(admin.TabularInline):
    model = Schedule
    exclude = (
            'sold_out',
            )
    extra = 1
    ordering = (
            'showtime',
            )

    class Media:
        js = ("reservations/js/admin_inlines.js",)
        css = {
            'all': ("reservations/css/admin_styles.css",)
            }


class PerformanceProxyAdmin(admin.ModelAdmin):
    """
    Schedule shows
    Показы (управление)
    """
    inlines = [
            ScheduleInline,
            ]
    save_on_top = True
    fieldsets = (
            (None, {
                'fields': ('title',),
                },
                ),
            )
    change_form_template = "reservations/admin_change_form.html"
    change_list_template = "reservations/admin_change_list.html"

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False



class ReservationInline(admin.TabularInline):
    model = Reservation
    extra = 0
    fields = ('reference_number',
            'reservation_date',
            'spectator',
            'seating_adult',
            'seating_child',
              'total_cost',
            'settled',
            'cancelled',
            'expired')
    readonly_fields = ('reservation_date', 'spectator', 'total_cost',
            'reference_number', 'expired', 'seating_adult', 'seating_child')


    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

class ScheduleAdmin(admin.ModelAdmin):
    """
    Расписание
    """
    model = Schedule
    fields = ( 'performance', 'place', 'showtime','sold_out')
    readonly_fields = ('performance', 'place', 'showtime')
    inlines = (ReservationInline,)
    date_hierarchy = 'showtime'
    list_filter = ['performance', 'place']
    list_display = (
            'showtime',
            'performance',
            'place',
            'reservations_made',
            'is_available',
            )
    actions = ['mark_sold_out']

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def mark_sold_out(self, request, queryset):
        queryset.update(sold_out=True)
    mark_sold_out.short_description = "Билеты кончились (убрать из афиши)"

class PlaceManagerAdmin(admin.ModelAdmin):
    fields = (
            "user",
            "first_name",
            "last_name",
            "email",
            "tel",
            )
    readonly_fields = (
            "first_name",
            "last_name",
            "email",
            )


admin.site.register(Place)
admin.site.register(Performance)
admin.site.register(PlaceManager, PlaceManagerAdmin)

admin.site.register(PerformanceProxy, PerformanceProxyAdmin)

admin.site.register(Schedule, ScheduleAdmin)
