# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PlaceManager'
        db.create_table(u'reservations_placemanager', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
            ('tel', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
        ))
        db.send_create_signal(u'reservations', ['PlaceManager'])

        # Adding model 'Performance'
        db.create_table(u'reservations_performance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('default_price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('thumb', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('brief', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('placeholder_field', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cms.Placeholder'], null=True)),
        ))
        db.send_create_signal(u'reservations', ['Performance'])

        # Adding model 'Place'
        db.create_table(u'reservations_place', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('thumb', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('manager', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'+', null=True, on_delete=models.SET_NULL, to=orm['reservations.PlaceManager'])),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
            ('how_to_get', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('pay_instructions', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('tickets_instructions', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('brief', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('placeholder_field', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cms.Placeholder'], null=True)),
        ))
        db.send_create_signal(u'reservations', ['Place'])

        # Adding model 'Spectator'
        db.create_table(u'reservations_spectator', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=256)),
            ('tel', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('registered', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('children', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True, blank=True)),
        ))
        db.send_create_signal(u'reservations', ['Spectator'])

        # Adding model 'Children'
        db.create_table(u'reservations_children', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'reservations', ['Children'])

        # Adding model 'Schedule'
        db.create_table(u'reservations_schedule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('performance', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'shows_scheduled', to=orm['reservations.Performance'])),
            ('place', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'shows_scheduled', to=orm['reservations.Place'])),
            ('showtime', self.gf('django.db.models.fields.DateTimeField')()),
            ('price', self.gf('django.db.models.fields.DecimalField')(null=True, max_digits=8, decimal_places=2, blank=True)),
            ('sold_out', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'reservations', ['Schedule'])

        # Adding model 'Promo'
        db.create_table(u'reservations_promo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('source', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal(u'reservations', ['Promo'])

        # Adding model 'Reservation'
        db.create_table(u'reservations_reservation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('spectator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reservations.Spectator'])),
            ('reservation_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('show', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'reservations', to=orm['reservations.Schedule'])),
            ('seating', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('promo_source', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reservations.Promo'], null=True, blank=True)),
            ('settled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('cancelled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('additional_info', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'reservations', ['Reservation'])

        # Adding model 'Review'
        db.create_table(u'reservations_review', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('review_date', self.gf('django.db.models.fields.DateField')()),
            ('spectator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reservations.Spectator'])),
            ('performance', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'reviews', null=True, to=orm['reservations.Performance'])),
            ('place', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name=u'reviews', null=True, to=orm['reservations.Place'])),
            ('text', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'reservations', ['Review'])

        # Adding model 'ReservationPlugin'
        db.create_table(u'reservations_reservationplugin', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('performance', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reservations.Performance'], null=True, blank=True)),
            ('place', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['reservations.Place'], null=True, blank=True)),
        ))
        db.send_create_signal(u'reservations', ['ReservationPlugin'])

        # Adding model 'SchedulePlugin'
        db.create_table(u'reservations_scheduleplugin', (
            (u'cmsplugin_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['cms.CMSPlugin'], unique=True, primary_key=True)),
            ('number', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=12)),
        ))
        db.send_create_signal(u'reservations', ['SchedulePlugin'])


    def backwards(self, orm):
        # Deleting model 'PlaceManager'
        db.delete_table(u'reservations_placemanager')

        # Deleting model 'Performance'
        db.delete_table(u'reservations_performance')

        # Deleting model 'Place'
        db.delete_table(u'reservations_place')

        # Deleting model 'Spectator'
        db.delete_table(u'reservations_spectator')

        # Deleting model 'Children'
        db.delete_table(u'reservations_children')

        # Deleting model 'Schedule'
        db.delete_table(u'reservations_schedule')

        # Deleting model 'Promo'
        db.delete_table(u'reservations_promo')

        # Deleting model 'Reservation'
        db.delete_table(u'reservations_reservation')

        # Deleting model 'Review'
        db.delete_table(u'reservations_review')

        # Deleting model 'ReservationPlugin'
        db.delete_table(u'reservations_reservationplugin')

        # Deleting model 'SchedulePlugin'
        db.delete_table(u'reservations_scheduleplugin')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'reservations.children': {
            'Meta': {'object_name': 'Children'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'reservations.performance': {
            'Meta': {'ordering': "[u'title']", 'object_name': 'Performance'},
            'brief': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'default_price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'placeholder_field': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'reservations.place': {
            'Meta': {'ordering': "[u'title']", 'object_name': 'Place'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'brief': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'how_to_get': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manager': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'+'", 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': u"orm['reservations.PlaceManager']"}),
            'pay_instructions': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'placeholder_field': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'thumb': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'tickets_instructions': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'reservations.placemanager': {
            'Meta': {'object_name': 'PlaceManager'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tel': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        },
        u'reservations.promo': {
            'Meta': {'object_name': 'Promo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        u'reservations.reservation': {
            'Meta': {'ordering': "[u'-reservation_date']", 'object_name': 'Reservation'},
            'additional_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'cancelled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'promo_source': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reservations.Promo']", 'null': 'True', 'blank': 'True'}),
            'reservation_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'seating': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'settled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'show': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'reservations'", 'to': u"orm['reservations.Schedule']"}),
            'spectator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reservations.Spectator']"})
        },
        u'reservations.reservationplugin': {
            'Meta': {'object_name': 'ReservationPlugin', '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'performance': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reservations.Performance']", 'null': 'True', 'blank': 'True'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reservations.Place']", 'null': 'True', 'blank': 'True'})
        },
        u'reservations.review': {
            'Meta': {'ordering': "[u'-review_date']", 'object_name': 'Review'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'performance': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'reviews'", 'null': 'True', 'to': u"orm['reservations.Performance']"}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "u'reviews'", 'null': 'True', 'to': u"orm['reservations.Place']"}),
            'review_date': ('django.db.models.fields.DateField', [], {}),
            'spectator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['reservations.Spectator']"}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'reservations.schedule': {
            'Meta': {'ordering': "[u'showtime']", 'object_name': 'Schedule'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'performance': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'shows_scheduled'", 'to': u"orm['reservations.Performance']"}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'shows_scheduled'", 'to': u"orm['reservations.Place']"}),
            'price': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '2', 'blank': 'True'}),
            'showtime': ('django.db.models.fields.DateTimeField', [], {}),
            'sold_out': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'reservations.scheduleplugin': {
            'Meta': {'object_name': 'SchedulePlugin', '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'number': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '12'})
        },
        u'reservations.spectator': {
            'Meta': {'ordering': "[u'-registered']", 'object_name': 'Spectator'},
            'children': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '256'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'registered': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'tel': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['reservations']