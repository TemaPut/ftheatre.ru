#!/usr/bin/env python
#vi:encoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals


from django.core.urlresolvers import reverse
from menus.base import NavigationNode
from menus.menu_pool import menu_pool
from cms.menu_bases import CMSAttachMenu

from reservations import models as m

class PlaceMenu(CMSAttachMenu):

    name = "Меню площадок"

    def get_nodes(self, request):
        nodes = []
        for place in m.Place.objects.filter(hide=False).reverse():
            node = NavigationNode(
                    place,
                    place.get_absolute_url(),
                    place.pk,
                    )
            nodes.append(node)
        return nodes

class PerformanceMenu(CMSAttachMenu):

    name = "Меню спектаклей"

    def get_nodes(self, request):
        nodes = []
        for perf in m.Performance.objects.filter(hide=False):
            node = NavigationNode(
                    perf,
                    perf.get_absolute_url(),
                    perf.pk,
                    )
            nodes.append(node)
        return nodes

menu_pool.register_menu(PlaceMenu)
menu_pool.register_menu(PerformanceMenu)
