��    >        S   �      H     I     M     [     c     j     o     t     �     �  	   �     �     �     �     �     �  
   �     �     �     �          	       "     #   9  
   ]     h     |     �     �      �     �     �     �     �  1        K     Z     i     o     x     ~     �  .   �     �     �  1   �  Q   �     K  I   X     �     �     �     �     �     �  '   �     �      	     	  	   
	     	  @  	     [
     l
  
   �
  
   �
  
   �
     �
  $   �
     �
     �
     �
  *   �
          -  #   F     j     w     �  
   �     �     �     �  
   �  d   �  d   F  '   �     �     �  0     I   3  H   }     �  &   �       Q     Z   e  %   �  %   �  
          
   (     3  
   <  j   G     �     �  �   �  �   h     �  u        �     �     �     �     �     �  Y   �     L     Y     `     q     x     :       3              '            	   %           !   .          /             (   4      =   #                           ;                              5          2       "   9      1   0   
      $      <   ,              7   )             *   >              &       +   8      -          6       Add Add Blog Post Archive Author Back Blog Blog Archive Blog Authors Blog Tag Blog Tags Blog posts on %(site_name)s Delete? Duration Edit Blog Post Engine Entries by Fade Feature File Gallery History Home If present image will be clickable If present image will be clickable. Key Visual Latest Blog Entries Lead-in No entry found. Please correct the error below. Please correct the errors below. Published Since Published Until Remove Set to 0 to disable autoplay Show only the blog posts tagged with chosen tags. Shuffle Slides Shuffle slides Slide Standard Style Tags Teaser The number of latests entries to be displayed. Timeout Title Used in the URL. If changed, the URL will change. Used in the URL. If changed, the URL will change. Clean it to have it re-created. View on site Will be displayed in lists, and at the start of the detail page (in bold) by description file file missing! image language leave empty to display in all languages link more page read more title Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-05-26 01:10+0400
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Добавить Добавить пост Архив Автор Назад Блог Архив записей блога Авторы Тэг Тэги Записи на сайте %(site_name)s Удалить? Длительность Редактировать пост Движок Автор записей Затенение Опция Файл Галерея История Домой Если указать, то изображение будет работать как ссылка Если указать, то изображение будет работать как ссылка Ключевое изображение Последние записи Вводная Не найдено ни одной записи Пожалуйста, исправьте следующую ошибку. Пожалуйста, исправьте следующие ошибки Опубликован Отображать в течение Удалить Чтобы отменить автопролистывание оставьте 0 Показать только посты, отмеченные данными тэгами Перетасовать слайды Перетасовать слайды Слайд Стандарт Стиль Тэги Тизер Количество последних сообщений, которые будут отображены Задержка Название Используется для формирования адреса URL. Адрес изменится, если изменить это поле Используется в адресе URL. Генерируется автоматически, еслиоставить поле пустым Открыть на сайте Будет отображаться п/ж в списке постов и в начале страницы поста автор:  описание файл файл не найден! изображение язык не заполняйте, чтобы отображалось на всех языках ссылка еще страница еще Название 