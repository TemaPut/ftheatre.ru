#!/usr/bin/env python
# vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from __future__ import print_function


from cms.models.pluginmodel import CMSPlugin
from django.db import models

# Create your models here.


class BootstrapEmbed(CMSPlugin):
    embed_url = models.CharField(
        "URL видео",
        help_text="например, //www.youtube.com/embed/l_C8q6oL2iE",
        max_length=250)
