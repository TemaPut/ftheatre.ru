#!/usr/bin/env python
# vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from __future__ import print_function


from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from bootstrap_embed_plugin.models import BootstrapEmbed


class BoostrapEmbedPlugin(CMSPluginBase):
    model = BootstrapEmbed
    name = "Встроенное видео"
    render_template = "bootstrap_embed.html"


print ("registering new plugin")
plugin_pool.register_plugin(BoostrapEmbedPlugin)
