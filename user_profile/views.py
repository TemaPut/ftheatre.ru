#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from logging import getLogger
log = getLogger("user_profile.views")

from django.shortcuts import resolve_url
from django.utils.http import  is_safe_url
from django.utils.module_loading import import_by_path
from django.conf import settings
from django.views.generic.edit import FormView 
from user_profile.forms import EmailRegisterForm
from django.contrib.auth import login, authenticate, get_user_model
from django.core.urlresolvers import reverse
from django.utils.six.moves.urllib.parse import urlparse, urlunparse
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode 
from django.http import QueryDict

User = get_user_model()

def build_register_link(next, 
        register_path='register',
        redirect_field_name="next",
        **kwargs):
    register_url = resolve_url(register_path)
    register_url_parts = list(urlparse(register_url))
    if redirect_field_name:
        querystring = QueryDict(register_url_parts[4], mutable=True)
        querystring[redirect_field_name] = next
        for key in kwargs:
            querystring[key] = urlsafe_base64_encode(kwargs[key])
        register_url_parts[4] = querystring.urlencode(safe='/')
        log.debug("next=%s\nregister_url=%s\nregister_link=%s",
                next, register_url, urlunparse(register_url_parts))
        return urlunparse(register_url_parts)
    else:
        return register_url




class RegisterView(FormView):
    template_name = "registration/register.html"
    form_class = EmailRegisterForm
    redirect_field_name = "next"

    def dispatch(self, request, *args, **kwargs):
        self.redirect_to = self.request.REQUEST.get(
                self.redirect_field_name, '') 
        return super(RegisterView, self).dispatch(
               request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RegisterView, self).get_context_data(**kwargs)
        context[self.redirect_field_name] = self.redirect_to
        return context

    def get_form(self, formclass):
        form = super(RegisterView, self).get_form(formclass)
        email = self.request.GET.get("email", "")
        if email:
            form.set_email(urlsafe_base64_decode(email))
        return form

    def get_success_url(self):
        # Ensure the user-originating redirection url is safe.
        redirect_to = self.redirect_to
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

        return redirect_to

    def form_valid(self, form):
        # we need to login created user before redirecting

        user = form.save()

        log.debug("user.email %s", user.email)
        user = self.user = authenticate(
                email=user.email,
                password=form.cleaned_data["password1"],
                )
        login(self.request, user) 
        return super(RegisterView, self).form_valid(form)
        


