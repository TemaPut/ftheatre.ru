#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from logging import getLogger
log = getLogger("user_profile.authentication")

from django.contrib.auth.backends import ModelBackend
from django.contrib.auth import get_user_model

class EmailAuthenticationBackend(ModelBackend):
    """
    Authenticates against settings.AUTH_USER_MODEL.
    """

    def authenticate(self, email=None, password=None, **kwargs):
        UserModel = get_user_model()
        log.debug("authenticating with email %s, pass %s", email, password)
        try:
            user = UserModel._default_manager.get(email=email)
            log.debug("user found")
            if user.check_password(password):
                return user
        except UserModel.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a non-existing user (#20760).
            UserModel().set_password(password)

