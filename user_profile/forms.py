#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from logging import getLogger
log = getLogger("user_profile.forms")

import string
import random

from django import forms
from django.contrib.auth import get_user_model, authenticate

User = get_user_model()

def generate_username():
    uname = ''.join([random.choice(string.letters + string.digits + '_')
                     for i in range(30)])
    User = get_user_model()
    try:
        User.objects.get(username=uname)
        return generate_username()
    except User.DoesNotExist:
        return uname

class EmailRegisterForm(forms.ModelForm):
    email = forms.EmailField(
            label = "Ваша эл. почта",
            error_messages = {
                "required": "Обязательно укажите свою эл. почту",
                }
            )
    password1 = forms.CharField(
            widget = forms.PasswordInput,
            label = "Новый пароль",
            )
    password2 = forms.CharField(
            widget = forms.PasswordInput,
            label = "Введите пароль еще раз",
            )


    class Meta:
        model = User 
        fields = ("email",)

    def set_email(self, email):
        self.fields["email"].initial = email
        self.fields["email"].widget.attrs = {
                    "readonly": "readonly",
                    }

    def clean_email(self):
        """
        disallow duplicate emails
        """
        email = self.cleaned_data["email"]
        if User._default_manager.filter(
                email=email).exists():
            raise forms.ValidationError(
                    "Пользователь с таким адресом уже зарегистрирован"
                    )
        return email


    def clean_password2(self):
        password1 = self.cleaned_data.get("password1", None)
        password2 = self.cleaned_data.get("password2", None)
        if password2 != password1:
            raise forms.ValidationError(
                    "Пароли не совпадают"
                    )
        return password2 
    
    def save(self, commit=True):
        return User._default_manager.create_user(
                generate_username(),
                email=self.cleaned_data["email"],
                password=self.cleaned_data["password1"]
                )
            


class EmailAuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    email = forms.EmailField(
            label="Ваша эл. почта",
            max_length=254,
            )
    password = forms.CharField(label="Пароль", widget=forms.PasswordInput)

    error_messages = {
        'invalid_login': "Пожалуйста, введите правильную эл. почту и пароль"
                           "Пароль должен вводиться с учетом регистра",
        'inactive': "Ваш аккаунт заблокирован администратором",
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(EmailAuthenticationForm, self).__init__(*args, **kwargs)

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email and password:
            self.user_cache = authenticate(email=email,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                )
            elif not self.user_cache.is_active:
                raise forms.ValidationError(
                    self.error_messages['inactive'],
                    code='inactive',
                )
        return self.cleaned_data

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache
