from django.conf.urls import *
from django.contrib.auth.decorators import login_required

from user_profile import views
urlpatterns = patterns('',
    url(r'^register/$', views.RegisterView.as_view(),
        name='register'),
    )

