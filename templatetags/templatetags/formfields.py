#!/usr/bin/env python
# vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals
from __future__ import print_function

from django import template

register = template.Library()


@register.inclusion_tag('partials/form_field_bootstrap.html')
def form_field_bootstrap(field, class_attr="", group_class_attr="",
                         placeholder="",
                         rows=0, has_label=True, label_class=""):
    if not hasattr(field, 'field'):
        return
    field_tag = field.field.widget.__class__.__name__.lower()
    if not placeholder and field_tag not in ('textarea',):
        placeholder = field.label
    class_attr = "form-control %s" % class_attr
    field.field.widget.attrs.update({
        'class': class_attr,
        'placeholder': placeholder,
    })
    if rows > 0:
        field.field.widget.attrs['rows'] = rows

    return {'field': field,
            'field_tag': field_tag,
            'has_label': has_label,
            'label_class': label_class,
            'group_class_attr': group_class_attr,
            }
