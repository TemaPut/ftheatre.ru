#!/usr/bin/env python
#vi:fileencoding=utf-8
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals


from os.path import join,  dirname
gettext = lambda s: s
"""
Django settings for ftheatre project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = dirname(dirname(__file__))
root = lambda x: join(BASE_DIR, x)


from os import environ


def get_env(name):
    try:
        return environ[name]
    except KeyError:
        pass
        # raise ImproperlyConfigured("Environment error: %s" % name)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

DEBUG = True
TEMPLATE_DEBUG = True
ERROR_TEST=False

# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': join(BASE_DIR, 'db.sqlite3'),
    }
}

# Application definition

# Allow all host headers
ALLOWED_HOSTS = ['*']
SECRET_KEY = get_env('SECRET_KEY')

ROOT_URLCONF = 'ftheatre.urls'

WSGI_APPLICATION = 'ftheatre.wsgi.application'


# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')


# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Moscow'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
MEDIA_URL = '/media/'
MEDIA_ROOT = join(BASE_DIR, 'media')
STATIC_ROOT = 'staticfiles'
STATICFILES_DIRS = (
    join(BASE_DIR, 'ftheatre', 'static'),
)
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
)
COMPRESS_PRECOMPILERS = (
    ('text/less', 'lessc {infile}'),
)

SITE_ID = 1

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.yandex.ru'
EMAIL_PORT = 587
EMAIL_USE_TLS = True


# ========================================
# common settings
# ========================================

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.doc.XViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'cms.middleware.user.CurrentUserMiddleware',
    'cms.middleware.page.CurrentPageMiddleware',
    'cms.middleware.toolbar.ToolbarMiddleware',
    'cms.middleware.language.LanguageCookieMiddleware'
)

AUTHENTICATION_BACKENDS = (
        'user_profile.authentication.EmailAuthenticationBackend',
        'django.contrib.auth.backends.ModelBackend',
        )

LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = 'reservations:profile'

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.debug',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.csrf',
    'django.core.context_processors.tz',
    'sekizai.context_processors.sekizai',
    'django.core.context_processors.static',
    'cms.context_processors.cms_settings'
)

TEMPLATE_DIRS = (
    join(BASE_DIR, 'ftheatre', 'templates'),
)

INSTALLED_APPS = (
    'djangocms_admin_style',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.sitemaps',
    'django.contrib.staticfiles',
    'django.contrib.messages',
    'cms',
    'djangocms_text_ckeditor',
    'mptt',
    'menus',
    'south',
    'sekizai',
    'djangocms_style',
    'djangocms_column',
    'djangocms_file',
    'djangocms_flash',
    'djangocms_googlemap',
    'djangocms_inherit',
    'djangocms_link',
    'djangocms_picture',
    'djangocms_teaser',
    'djangocms_video',
    'bootstrap_embed_plugin',
    'ftheatre',
    'reservations',
    'user_profile',
    'aldryn_blog',
    'aldryn_common',
    'django_select2',
    'easy_thumbnails',
    'filer',
    'taggit',
    'aldryn_gallery',
    #'debug_toolbar',
    'templatetags',
    'hvad',
    'compressor'
)


SOUTH_MIGRATION_MODULES = {
        'easy_thumbnails': 'easy_thumbnails.south_migrations',
    }
LANGUAGES = (
    ## Customize this
    ('ru', gettext('ru')),
)

LOCALE_PATHS = (
        root('locale'),
        )
CMS_LANGUAGES = {
    ## Customize this
    'default': {
        'public': True,
        'hide_untranslated': False,
        'redirect_on_fallback': True,
    },
    1: [
        {
            'public': True,
            'code': 'ru',
            'hide_untranslated': False,
            'name': gettext('ru'),
            'redirect_on_fallback': True,
        },
    ],
}

CMS_TEMPLATES = (
    ## Customize this
    ('page.html', 'Page'),
    ('feature.html', 'Page with Feature'),
    ('sidebar.html', 'Page with Sidebar'),
)

CMS_PERMISSION = True

CMS_REDIRECTS = True

CMS_PLACEHOLDER_CONF = {
    'main_header_image': {
        'plugins': ['PicturePlugin'],
        'name': 'Заглавная картинка',
    },
    'header_image': {
        'plugins': ['PicturePlugin'],
        'name': 'Заглавная картинка',
    },
    'aside': {
        'name': 'Боковик',
    },
    'cellar': {
        'name': 'Подвал',
    },
}

CKEDITOR_SETTINGS = {
    'language': 'ru',
    'toolbar_CMS': [
    ],
    'toolbar_HTMLField': [
    ],
}

THUMBNAIL_PROCESSORS = (
    'easy_thumbnails.processors.colorspace',
    'easy_thumbnails.processors.autocrop',
    #'easy_thumbnails.processors.scale_and_crop',
    'filer.thumbnail_processors.scale_and_crop_with_subject_location',
    'easy_thumbnails.processors.filters',
    'easy_thumbnails.processors.background',
)

# ========================================
# for local run replace everything from local.py
# ========================================
try:
    from ftheatre.local import *  # noqa
except ImportError:
    pass

from .reservations_conf import *  # noqa
