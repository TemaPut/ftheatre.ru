from django.conf.urls import *
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from django.views.generic import RedirectView
from django.core.urlresolvers import reverse_lazy
from cms.sitemaps import CMSSitemap
from user_profile.forms import EmailAuthenticationForm

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/profile/', include('user_profile.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {
        'authentication_form': EmailAuthenticationForm,
            },
        name='login'),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^playbill/', include('reservations.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^select2/', include('django_select2.urls')),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': {'cmspages': CMSSitemap}}),
                       )

# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
) + staticfiles_urlpatterns() + urlpatterns
if settings.ERROR_TEST:
    # We add some bullshit address just to check that errors are reported
    urlpatterns += patterns('',
            url(r'^500errortest/$','error_test.raise_error')
            )
# !CMS url
urlpatterns += patterns('',
    url(r'^', include('cms.urls')),
            )
