RESERVATIONS_EXPIRES_BEFORE = 2  # How long before the night of the show should it be considered expired

USER_PROFILE_MODEL = 'reservations.models.Spectator'
