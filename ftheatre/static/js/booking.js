var ftheatre = (function ftheatre_namespace(o, $) {
    "use strict";

    var BookingTotalCalc = function(element, options) {
        //Constructor: find totals div, get prices, await for seating changes
        this.$el = $(element)
        this.$display = this.$el.find(".display")
        this.options = $.extend({}, BookingTotalCalc.defaults, options)
        var prices = {}
        this.$el.find(".prices>li").each(function() {
            prices[$(this).data("pk")] = {
                price_adult: Number($(this).data("priceAdult")),
                price_child: Number($(this).data("priceChild")),
                price_default: Number($(this).data("priceDefault")),
            }
        })
        this.prices = prices

        this.form = this.$el.closest("form").get(0)
        $(this.form.seating_adult).change($.proxy(this.redraw, this))
        $(this.form.seating_child).change($.proxy(this.redraw, this))
        $(this.form.show).change($.proxy(this.redraw, this))
    }
    BookingTotalCalc.prototype = {
        redraw: function() {
            console.log("fire")
            this.$display.text(this.calculate())
        },

        get_prices: function() {
            var show_pk = $(this.form.show).find(":selected").val()

            return this.prices[show_pk]
        },

        calculate: function() {
            var prices = this.get_prices()    
            return prices.price_default + prices.price_child * (
                this.form.seating_child.value - 1) + prices.price_adult * (
                this.form.seating_adult.value - 1)
        },


    }

    BookingTotalCalc.defaults = {}

    $(function() {
        new BookingTotalCalc($('.total-cost').get(0))
    })







    return o

})(ftheatre || {}, jQuery)
