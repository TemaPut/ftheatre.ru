var ftheatre = (function ftheatre_namespace(o, $) {
    "use strict";

    var BookingFormAjah = function(element, options) {
        this.$element = element;
        this.form = this.$element.get(0);
        this.cache_data = {};
        this.options = $.extend({}, BookingFormAjah.defaults, options);
        this.bind_events();
    };
    BookingFormAjah.prototype = {

        bind_events: function() {
            $(this.form.elements.performance).change({
                functors: BookingFormAjah.place_functors,
            },
            $.proxy(function(e) {
                this.update("place", e.data.functors);
            }, this));

            $(this.form).submit($.proxy(function(e) {
                if (this.field_is_hidden("performance")) {
                    e.preventDefault();
                    this.show_field("performance");
                    this.rename_submit("Продолжить бронирование");
                } else if (this.field_is_hidden("place")) {
                    e.preventDefault();
                    this.update("place", BookingFormAjah.place_functors);
                    this.rename_submit("Продолжить бронирование");
                }

            }, this));



        },

        field_is_hidden: function(field_name) {
            return !$(this.form.elements[field_name]).closest(".form-group").hasClass(
               "in");
        },

        show_field: function(field_name) {
            $(this.form.elements[field_name]).closest(".form-group").collapse('show');
        },

        rename_submit: function(new_name) {
            $(this.form).find(":button").text(new_name);
        },

        update: function(field, functors) {
            var filter = functors.filter_func(this.form);
            var hash = functors.hash_func(filter);
            if (this.cache_data[hash]) {
                this.update_from_cache(field, hash);
            } else {
                this.update_from_server(field, filter, hash, functors.repr_func);
            }
        },

        update_from_server: function(field, filter, hash, repr_func) {
            var api_url = this.form.action + this.options.api_url + field + "s/";
            var cache_data = this.cache_data;
            var callback = $.proxy(
                function() { this.update_from_cache(field, hash); },
                this);
            $.getJSON(api_url, filter, function( data ) {
                var items = cache_data[hash] = [];
                $.each( data, function( key, val ) {
                    var repr = repr_func(val);
                    items.push(repr);
                });
                callback();
            });
        },


        update_from_cache: function(field, hash) {
            var items = this.cache_data[hash];
            $(this.form.elements[field]).html(
                items.join("")
            );
            if (this.field_is_hidden(field))
                this.show_field(field);
        },



    };

    BookingFormAjah.place_functors = {

                filter_func: function(form) {
                    return {performance: $(form.elements.performance).val()};
                },
                hash_func: function(filter) {
                    return filter.performance;
                },
                repr_func: function(json_data) {
                    var title = "Площадка " + json_data.fields.title;
                    var pk = json_data.pk;
                    return '<option value="' + pk + '">' + title + '</option>';
                },
    };

    BookingFormAjah.defaults = {
        api_url: "api/",
    };



    $(function() {
        o.bfa = new BookingFormAjah($('form#preorder-form'));
    });

    return o;

})(ftheatre || {}, jQuery);
